@extends('layouts.admin', [
  'page_header' => 'Students',
  'dash' => '',
  'quiz' => '',
  'users' => 'active',
  'questions' => '',
  'top_re' => '',
  'all_re' => '',
  'sett' => ''
])
@section('content')
    @if ($auth->role == 'A')

        <?php
        $division = App\Division::pluck('division', 'id');
        $classes = App\SchoolClasses::pluck('roman_value', 'id');
        $schools = App\School::pluck('name', 'code');

        ?>
        <div class="margin-bottom">
            <button type="button" class="btn btn-wave" data-toggle="modal" data-target="#createModal">Add Student
            </button>
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#AllDeleteModal">Delete All
                Students
            </button>
        </div>
        <!-- All Delete Button -->
        <div id="AllDeleteModal" class="delete-modal modal fade" role="dialog">
            <!-- All Delete Modal -->
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="delete-icon"></div>
                    </div>
                    <div class="modal-body text-center">
                        <h4 class="modal-heading">Are You Sure ?</h4>

                        <p>Do you really want to delete "All these records"? This process cannot be undone.</p>
                    </div>
                    <div class="modal-footer">
                        {!! Form::open(['method' => 'POST', 'action' => 'DestroyAllController@AllUsersDestroy']) !!}
                        {!! Form::reset("No", ['class' => 'btn btn-gray', 'data-dismiss' => 'modal']) !!}
                        {!! Form::submit("Yes", ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- Create Modal -->
        <div id="createModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Student</h4>
                    </div>
                    {!! Form::open(['method' => 'POST', 'action' => 'UsersController@store']) !!}
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    {!! Form::label('name', 'Student Name') !!}
                                    <span class="required">*</span>
                                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Name']) !!}
                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                </div>

                                <div class="form-group{{ $errors->has('school_code') ? ' has-error' : '' }}">
                                    {!! Form::label('school_code', 'School Name') !!}
                                    <span class="required">*</span>
                                    {!! Form::select('school_code',$schools, null, ['class' => 'form-control select2', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('school_code') }}</small>
                                </div>

                                <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
                                    {!! Form::label('class', 'Class name') !!}
                                    <span class="required">*</span>
                                    {!! Form::select('class',$classes, null, ['class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('class') }}</small>
                                </div>

                                <div class="form-group{{ $errors->has('div') ? ' has-error' : '' }}">
                                    {!! Form::label('div', 'Division Name') !!}
                                    <span class="required">*</span>
                                    {!! Form::select('div',$division, null, ['class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('div') }}</small>
                                </div>

                                <div class="form-group{{ $errors->has('roll_no') ? ' has-error' : '' }}">
                                    {!! Form::label('roll_no', 'Roll No') !!}
                                    <span class="required">*</span>
                                    {!! Form::text('roll_no', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Roll no']) !!}
                                    <small class="text-danger">{{ $errors->first('roll_no') }}</small>
                                </div>
                                <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                                    {!! Form::label('father_name', 'Father Name') !!}
                                    <span class="required">*</span>
                                    {!! Form::text('father_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Father Name']) !!}
                                    <small class="text-danger">{{ $errors->first('father_name') }}</small>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group{{ $errors->has('father_contact') ? ' has-error' : '' }}">
                                    {!! Form::label('father_contact', 'Father Contact') !!}
                                    <span class="required">*</span>
                                    {!! Form::text('father_contact', null, ['class' => 'form-control','placeholder' => 'Enter Father Contact']) !!}
                                    <small class="text-danger">{{ $errors->first('father_contact') }}</small>
                                </div>
                                <div class="form-group{{ $errors->has('father_email') ? ' has-error' : '' }}">
                                    {!! Form::label('father_email', 'Father Email') !!}
                                    <span class="required">*</span>
                                    {!! Form::email('father_email', null, ['class' => 'form-control','placeholder' => 'Enter Father Email']) !!}
                                    <small class="text-danger">{{ $errors->first('father_email') }}</small>
                                </div>

                                <div class="form-group{{ $errors->has('mother_name') ? ' has-error' : '' }}">
                                    {!! Form::label('mother_name', 'Mother Name') !!}
                                    <span class="required">*</span>
                                    {!! Form::text('mother_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Mother Name']) !!}
                                    <small class="text-danger">{{ $errors->first('mother_name') }}</small>
                                </div>

                                <div class="form-group{{ $errors->has('mother_contact') ? ' has-error' : '' }}">
                                    {!! Form::label('mother_contact', 'Mother Contact') !!}
                                    <span class="required">*</span>
                                    {!! Form::text('mother_contact', null, ['class' => 'form-control', 'placeholder' => 'Enter Mother Contact']) !!}
                                    <small class="text-danger">{{ $errors->first('mother_contact') }}</small>
                                </div>
                                <div class="form-group{{ $errors->has('mother_email') ? ' has-error' : '' }}">
                                    {!! Form::label('mother_email', 'Mother Email') !!}
                                    <span class="required">*</span>
                                    {!! Form::email('mother_email', null, ['class' => 'form-control', 'placeholder' => 'Enter Mother Email']) !!}
                                    <small class="text-danger">{{ $errors->first('mother_email') }}</small>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group pull-right">
                            {!! Form::reset("Reset", ['class' => 'btn btn-default']) !!}
                            {!! Form::submit("Add", ['class' => 'btn btn-wave']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="content-block box">
            <div class="box-body table-responsive">
                <table id="example1" class="table table-striped">
                    <thead>
                    <tr>
                        <th>Sr.no</th>
                        <th>Student Name</th>
                        <th>School Code</th>
                        <th>School Name</th>
                        <th>Class</th>
                        <th>Divisions</th>
                        <th>Roll No</th>
                        <th>Father Name</th>
                        <th>Mother Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($users)
                        @php($n = 1)
                        @foreach ($users as $key => $user)
                            <tr>
                                <td>
                                    {{$n}}
                                    @php($n++)
                                </td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->school_code}}</td>
                                <td>{{$user->schoolCode->name}}</td>
                                <td>{{$user->schoolclasses->roman_value }}</td>
                                <td>{{$user->schooldiv->division}}</td>
                                <td>{{$user->roll_no}}</td>
                                <td>{{$user->father_name}}</td>
                                <td>{{$user->mother_name}}</td>
                                <td>
                                    <!-- Edit Button -->
                                    <a type="button" class="btn btn-info btn-xs" data-toggle="modal"
                                       data-target="#{{$user->id}}EditModal"><i class="fa fa-edit"></i> Edit</a>
                                    <!-- Delete Button -->
                                    <a type="button" class="btn btn-xs btn-danger" data-toggle="modal"
                                       data-target="#{{$user->id}}deleteModal"><i class="fa fa-close"></i> Delete</a>

                                    <div id="{{$user->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                                        <!-- Delete Modal -->
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    <div class="delete-icon"></div>
                                                </div>
                                                <div class="modal-body text-center">
                                                    <h4 class="modal-heading">Are You Sure ?</h4>

                                                    <p>Do you really want to delete these records? This process cannot
                                                        be undone.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(['method' => 'DELETE', 'action' => ['UsersController@destroy', $user->id]]) !!}
                                                    {!! Form::reset("No", ['class' => 'btn btn-gray', 'data-dismiss' => 'modal']) !!}
                                                    {!! Form::submit("Yes", ['class' => 'btn btn-danger']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- edit model -->
                            <div id="{{$user->id}}EditModal" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Student</h4>
                                        </div>
                                        {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UsersController@update', $user->id]]) !!}
                                        <div class="modal-body">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                        {!! Form::label('name', 'Student Name') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Name']) !!}
                                                        <small class="text-danger">{{ $errors->first('name') }}</small>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('school_code') ? ' has-error' : '' }}">
                                                        {!! Form::label('school_code', 'School Name') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::select('school_code',$schools, null, ['class' => 'form-control select2', 'required' => 'required']) !!}
                                                        <small class="text-danger">{{ $errors->first('school_code') }}</small>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
                                                        {!! Form::label('class', 'Class name') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::select('class',$classes, null, ['class' => 'form-control', 'required' => 'required']) !!}
                                                        <small class="text-danger">{{ $errors->first('class') }}</small>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('div') ? ' has-error' : '' }}">
                                                        {!! Form::label('div', 'Division Name') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::select('div',$division, null, ['class' => 'form-control', 'required' => 'required']) !!}
                                                        <small class="text-danger">{{ $errors->first('div') }}</small>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('roll_no') ? ' has-error' : '' }}">
                                                        {!! Form::label('roll_no', 'Roll No') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::text('roll_no', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Roll no']) !!}
                                                        <small class="text-danger">{{ $errors->first('roll_no') }}</small>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                                                        {!! Form::label('father_name', 'Father Name') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::text('father_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Father Name']) !!}
                                                        <small class="text-danger">{{ $errors->first('father_name') }}</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                    <div class="form-group{{ $errors->has('father_contact') ? ' has-error' : '' }}">
                                                        {!! Form::label('father_contact', 'Father Contact') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::text('father_contact', null, ['class' => 'form-control','placeholder' => 'Enter Father Contact']) !!}
                                                        <small class="text-danger">{{ $errors->first('father_contact') }}</small>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('father_email') ? ' has-error' : '' }}">
                                                        {!! Form::label('father_email', 'Father Email') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::email('father_email', null, ['class' => 'form-control','placeholder' => 'Enter Father Email']) !!}
                                                        <small class="text-danger">{{ $errors->first('father_email') }}</small>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('mother_name') ? ' has-error' : '' }}">
                                                        {!! Form::label('mother_name', 'Mother Name') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::text('mother_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Mother Name']) !!}
                                                        <small class="text-danger">{{ $errors->first('mother_name') }}</small>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('mother_contact') ? ' has-error' : '' }}">
                                                        {!! Form::label('mother_contact', 'Mother Contact') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::text('mother_contact', null, ['class' => 'form-control', 'placeholder' => 'Enter Mother Contact']) !!}
                                                        <small class="text-danger">{{ $errors->first('mother_contact') }}</small>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('mother_email') ? ' has-error' : '' }}">
                                                        {!! Form::label('mother_email', 'Mother Email') !!}
                                                        <span class="required">*</span>
                                                        {!! Form::email('mother_email', null, ['class' => 'form-control', 'placeholder' => 'Enter Mother Email']) !!}
                                                        <small class="text-danger">{{ $errors->first('mother_email') }}</small>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="btn-group pull-right">
                                                {!! Form::submit("Update", ['class' => 'btn btn-wave']) !!}
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection
