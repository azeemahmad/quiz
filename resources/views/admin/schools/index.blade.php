@extends('layouts.admin', [
  'page_header' => 'Quiz',
  'dash' => '',
  'quiz' => 'active',
  'users' => '',
  'questions' => '',
  'top_re' => '',
  'all_re' => '',
  'sett' => '',
  'school' => ''
])

@section('content')
  <div class="margin-bottom">
    <button type="button" class="btn btn-wave" data-toggle="modal" data-target="#createModal">Add School</button>
  </div>
  <!-- Create Modal -->
  <div id="createModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add School</h4>
        </div>
        {!! Form::open(['method' => 'POST', 'action' => 'SchoolController@store']) !!}
          <div class="modal-body">
            <div class="row">

              <div class="col-md-6">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  {!! Form::label('name', 'School Name') !!}
                  <span class="required">*</span>
                  {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School Name', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>
                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                  {!! Form::label('location', 'Location') !!}
                  <span class="required">*</span>
                  {!! Form::text('location', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School Location', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('location') }}</small>
                </div>
                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                  {!! Form::label('city', 'Enter City') !!}
                  {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School City']) !!}
                  <small class="text-danger">{{ $errors->first('city') }}</small>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                  {!! Form::label('state', 'Enter State') !!}
                  {!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School State']) !!}
                  <small class="text-danger">{{ $errors->first('state') }}</small>
                </div>
                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                  {!! Form::label('country', 'Enter Country') !!}
                  {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School State']) !!}
                  <small class="text-danger">{{ $errors->first('country') }}</small>
                </div>
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                  {!! Form::label('phone', 'Enter Phone') !!}
                  {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School Phone']) !!}
                  <small class="text-danger">{{ $errors->first('phone') }}</small>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <div class="btn-group pull-right">
              {!! Form::reset("Reset", ['class' => 'btn btn-default']) !!}
              {!! Form::submit("Add", ['class' => 'btn btn-wave']) !!}
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <div class="box">
    <div class="box-body table-responsive">
      <table id="search" class="table table-hover table-striped">
        <thead>
          <tr>
            <th>Sr.no</th>
            <th>Name</th>
            <th>Code</th>
            <th>Location</th>
            <th>City</th>
            <th>State</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @if ($topics)
            @php($i = 1)
            @foreach ($topics as $topic)
              <tr>
                <td>
                  {{$i}}
                  @php($i++)
                </td>
                <td>{{$topic->name}}</td>
                <td> {{$topic->code}}</td>
                <td>{{$topic->location}}</td>
                <td>{{$topic->city}}</td>
                <td>{{$topic->state}}</td>
                <td>
                  <!-- Edit Button -->
                  <a type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#{{$topic->id}}EditModal"><i class="fa fa-edit"></i> Edit</a>
                  <!-- Delete Button -->
                  <a type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#{{$topic->id}}deleteModal"><i class="fa fa-close"></i> Delete</a>
                  <div id="{{$topic->id}}deleteModal" class="delete-modal modal fade" role="dialog">
                    <!-- Delete Modal -->
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <div class="delete-icon"></div>
                        </div>
                        <div class="modal-body text-center">
                          <h4 class="modal-heading">Are You Sure ?</h4>
                          <p>Do you really want to delete these records? This process cannot be undone.</p>
                        </div>
                        <div class="modal-footer">
                          {!! Form::open(['method' => 'DELETE', 'action' => ['SchoolController@destroy', $topic->id]]) !!}
                            {!! Form::reset("No", ['class' => 'btn btn-gray', 'data-dismiss' => 'modal']) !!}
                            {!! Form::submit("Yes", ['class' => 'btn btn-danger']) !!}
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <!-- edit model -->
              <div id="{{$topic->id}}EditModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Quiz</h4>
                    </div>
                    {!! Form::model($topic, ['method' => 'PATCH', 'action' => ['SchoolController@update', $topic->id]]) !!}
                      <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::label('name', 'School Name') !!}
                                <span class="required">*</span>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School Name', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                              </div>
                              <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                {!! Form::label('location', 'Location') !!}
                                <span class="required">*</span>
                                {!! Form::text('location', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School Location', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('location') }}</small>
                              </div>
                              <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                {!! Form::label('city', 'Enter City') !!}
                                {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School City']) !!}
                                <small class="text-danger">{{ $errors->first('city') }}</small>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                {!! Form::label('state', 'Enter State') !!}
                                {!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School State']) !!}
                                <small class="text-danger">{{ $errors->first('state') }}</small>
                              </div>
                              <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                {!! Form::label('country', 'Enter Country') !!}
                                {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School State']) !!}
                                <small class="text-danger">{{ $errors->first('country') }}</small>
                              </div>
                              <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                {!! Form::label('phone', 'Enter Phone') !!}
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Please Enter School Phone']) !!}
                                <small class="text-danger">{{ $errors->first('phone') }}</small>
                              </div>
                            </div>
                          </div>
                        </div>
                      <div class="modal-footer">
                        <div class="btn-group pull-right">
                          {!! Form::submit("Update", ['class' => 'btn btn-wave']) !!}
                        </div>
                      </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection
