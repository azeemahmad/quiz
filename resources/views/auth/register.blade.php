@extends('layouts.app')

@section('head')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        window.Laravel =  <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
@endsection

@section('content')
    <?php
    $division = App\Division::pluck('division', 'id');
    $classes = App\SchoolClasses::pluck('roman_value', 'id');
    $schools = App\School::pluck('name', 'code');
    ?>
    <div class="bg-cream">
        <div class="container">
            <div class="login">
                <div class="logo">
                    @if ($setting)
                        <a href="{{url('/')}}" title="{{$setting->welcome_txt}}"><img
                                    src="{{asset('/images/logo/'.$setting->logo)}}" class="img-responsive login-logo"
                                    alt="{{$setting->welcome_txt}}"></a>
                    @endif
                </div>
                <h3 class="user-register-heading text-center">Register</h3>

                <form class="form login-form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::label('name', 'Student Name') !!}
                            <span class="required">*</span>
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Your Name']) !!}
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>

                        <div class="form-group{{ $errors->has('school_code') ? ' has-error' : '' }}">
                            {!! Form::label('school_code', 'School Name') !!}
                            <span class="required">*</span>
                            {!! Form::select('school_code',$schools, null, ['class' => 'form-control select2', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('school_code') }}</small>
                        </div>

                        <div class="form-group{{ $errors->has('class') ? ' has-error' : '' }}">
                            {!! Form::label('class', 'Class name') !!}
                            <span class="required">*</span>
                            {!! Form::select('class',$classes, null, ['class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('class') }}</small>
                        </div>

                        <div class="form-group{{ $errors->has('div') ? ' has-error' : '' }}">
                            {!! Form::label('div', 'Division Name') !!}
                            <span class="required">*</span>
                            {!! Form::select('div',$division, null, ['class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('div') }}</small>
                        </div>

                        <div class="form-group{{ $errors->has('roll_no') ? ' has-error' : '' }}">
                            {!! Form::label('roll_no', 'Roll No') !!}
                            <span class="required">*</span>
                            {!! Form::text('roll_no', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Your Roll no']) !!}
                            <small class="text-danger">{{ $errors->first('roll_no') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('father_name') ? ' has-error' : '' }}">
                            {!! Form::label('father_name', 'Father Name') !!}
                            <span class="required">*</span>
                            {!! Form::text('father_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Your Father Name']) !!}
                            <small class="text-danger">{{ $errors->first('father_name') }}</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('father_contact') ? ' has-error' : '' }}">
                            {!! Form::label('father_contact', 'Father Contact') !!}
                            <span class="required">*</span>
                            {!! Form::text('father_contact', null, ['class' => 'form-control','placeholder' => 'Enter Your Father Contact']) !!}
                            <small class="text-danger">{{ $errors->first('father_contact') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('father_email') ? ' has-error' : '' }}">
                            {!! Form::label('father_email', 'Father Email') !!}
                            <span class="required">*</span>
                            {!! Form::email('father_email', null, ['class' => 'form-control','placeholder' => 'Enter Your Father Email']) !!}
                            <small class="text-danger">{{ $errors->first('father_email') }}</small>
                        </div>

                        <div class="form-group{{ $errors->has('mother_name') ? ' has-error' : '' }}">
                            {!! Form::label('mother_name', 'Mother Name') !!}
                            <span class="required">*</span>
                            {!! Form::text('mother_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter Your Mother Name']) !!}
                            <small class="text-danger">{{ $errors->first('mother_name') }}</small>
                        </div>

                        <div class="form-group{{ $errors->has('mother_contact') ? ' has-error' : '' }}">
                            {!! Form::label('mother_contact', 'Mother Contact') !!}
                            <span class="required">*</span>
                            {!! Form::text('mother_contact', null, ['class' => 'form-control', 'placeholder' => 'Enter Your Mother Contact']) !!}
                            <small class="text-danger">{{ $errors->first('mother_contact') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('mother_email') ? ' has-error' : '' }}">
                            {!! Form::label('mother_email', 'Mother Email') !!}
                            <span class="required">*</span>
                            {!! Form::email('mother_email', null, ['class' => 'form-control', 'placeholder' => 'Enter Your Mother Email']) !!}
                            <small class="text-danger">{{ $errors->first('mother_email') }}</small>
                        </div>
                    </div>
                    <div class="col-md-4 pull-right">
                    <div class="mr-t-20">
                        <button type="submit" class="btn btn-wave btn-sm">Login / Register</button>
                        {{--<a href="{{url('/login')}}" class="text-center btn-block" title="Already Have Account">Already Have Account ?</a>--}}
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
