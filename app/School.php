<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name', 'location', 'city', 'state', 'code','phone','country'
    ];


}
