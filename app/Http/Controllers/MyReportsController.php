<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Topic;
use App\Answer;
use App\Question;

class MyReportsController extends Controller
{
    public function index()
    {
        if (Auth::check()) {

          $user=Auth::user();
  $topics = Topic::where('school_code',$user->school_code)->get(['id','class']);
   $topics->map(function ($query) use ($user) {   
          $query->class=explode(',', $query->class);        
   });
    $finaltopics=$topics->toArray();
    $class_ids=array();
    foreach ($finaltopics as $key => $value) {
         $class=$value['class'];
         if(in_array($user->class, $class)){
          $class_ids[]=$value['id'];
         }
    }
  $topics = Topic::whereIn('id',$class_ids)->get(); 
          $questions = Question::all();
          return view('admin.my_reports.index', compact('topics', 'questions'));

        } else {
          return redirect('/');
        }
    }

    public function show($id) {

      if (Auth::check()) {

        $auth = Auth::user();
        $topic = Topic::findOrFail($id);
        $total_marks = Question::where('topic_id', $topic->id)->get()->count();
        $answers = Answer::where('user_id', $auth->id)->where('topic_id', $topic->id)->get();

        return view('admin.my_reports.show', compact('topic', 'answers', 'total_marks'));

      } else {
        return redirect('/');
      }


    }
}
